# Lamp Mi Web
##Juan De la Cruz Quioz

## Subir Nuestra Imagen a Docker Hub

1. Iniciar sesion en Docker Hub desde un terminal "Debemos tener una cuenta en  https://hub.docker.com."

$ docker login -u docker-registry-username # Indicamos nuestro Usuario, luego nos pedira la contraseña de Docker Hub
## Para subir nuestra imagen Docker
$docker push docker-registry-username/docker-image-name:tag

```
2. Creamos el direcorio donde alojaremos nuestra web 
 $ sudo mkdir /var/www/html # Aqui alojaremos nuestra web Index.php 

3. Armamos el Cluster Kubernetes: 
## Creamos un directorio donde alojaremos los Archivos .yaml para crear el cluster
$ sudo mkdir ./Kubernetes # Aqui debemos descargar los arcivos para crear el cluster

### Archivos de referencia lampdocker-pv.yaml,lampdocker-rst.yaml,lampdocker-dp.yaml,lampdocker-sv.yaml

#### Crear al Cluster
$ kubectl apply -f Kubernetes/lampdoker-pv.yaml
$ kubectl apply -f Kubernetes/lampdoker-pv-claim.yaml
$ kubectl apply -f Kubernetes/lampdoker-data.yaml
```
4. Despues de crear los volumenes persistentes para el cluster continuamos con Deployment:

$ kubectl apply -f Kubernetes/lampdoker-dp.yaml
```

5. Continuamos creando el Servicio para poder acceder desde afuera.

$ kubectl apply -f Kubernetes/lampdoker-sv.yaml
```
6. Ya podemos acceder a nuestra web desde un navegador indicando la IP de la maquina virtual 
###Ejemplo: http://x.x.x.x

